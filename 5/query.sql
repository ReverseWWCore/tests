SELECT b.bookName FROM books b LEFT JOIN booksAuthors a ON b.bookId = a.bookId GROUP BY a.bookId HAVING COUNT(a.authorId) = 3 LIMIT 0, 1000;
-- получить названия книг у которых 3 со-автора
-- bookName             
-- ---------------------
-- book with 2 authors  
-- test book 2          