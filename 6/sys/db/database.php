<?php
/**
 * Created by PhpStorm.
 * User: Роман Аношкин
 * Date: 04.09.2017
 * Time: 19:13
 */
class Database
{
    var $connection = null;
    var $lastQuery = null;
    public function __construct($host, $user, $passwd, $db, $port)
    {
        if(!$this->connection)
            $this->connection =  mysqli_connect($host, $user, $passwd, $db, $port);
    }
    public function connectionExists()
    {
        return $this->connection != NULL;
    }
    public function query($query)
    {
        if(!$query || $query == "")
            return $lastQuery = null;
        return $this->lastQuery = mysqli_query($this->connection, $query);
    }
    public function fetch($query = null)
    {
        if($query == null)
            return mysqli_fetch_array($this->lastQuery);
        return mysqli_fetch_array($query);
    }
    public function numRows($query = null)
    {
        if($query == null) {
            if ($this->lastQuery == null)
                return 0;
            return mysqli_num_rows($this->lastQuery);
        }
        return mysqli_num_rows($query);
    }
    public function esc($arg)
    {
        return mysqli_real_escape_string($this->connection, htmlspecialchars($arg));
    }
}