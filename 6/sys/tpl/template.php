<?php
/**
 * Created by PhpStorm.
 * User: Роман Аношкин
 * Date: 04.09.2017
 * Time: 21:24
 */
class TemplateMgr
{
    var $output = '';
    var $loadedTpls = array();
    public function __construct()
    {
        $this->loadedTpls = array();
        $this->output = '';

    }
    public function Output($data)
    {
        echo $this->Put($data);
    }
    public function Put($data, $idx = 'index')
    {
        $put = (!is_array($data)) ? array('data' => $data) : $data;
        if(!isset($loadedTpls[$idx]))
            $this->Load($idx);
        if($this->loadedTpls[$idx] == '!#err#!')
            return '';
        if(isset($put['data']))
            if($put['data'] == '!#null#!')
                return $this->loadedTpls[$idx];
        $result = $this->loadedTpls[$idx];
        foreach($put as $id => $value)
            $result = str_replace('!#'. strtolower($id) .'#!', $value, $result);
        return $result;
    }
    public function Load($idx)
    {
        if(file_exists('./app/template/'. $idx .'.tpl'))
            $this->loadedTpls[$idx] = file_get_contents('./app/template/'. $idx .'.tpl');
        else
            $this->loadedTpls[$idx] = '!#err#!';
    }
}