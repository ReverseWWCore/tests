<?php
/**
 * Created by PhpStorm.
 * User: Роман Аношкин
 * Date: 04.09.2017
 * Time: 19:03
 */


require_once './sys/db/database.php';
require_once './sys/db/config.php';
$db = new Database($config['host'],$config['user'],$config['pass'],$config['base'],$config['port']);

if(!$db->connectionExists())
    exit('err, no db connection');
require_once './sys/tpl/template.php';
$tplMgr = new TemplateMgr();
$echo = '';
$arg = (isset($_GET['do'])) ? strtolower($_GET['do']) : '';

if($arg != '')
    if(!file_exists('./app/modules/'. $arg .'.php'))
        $arg = '';
if($arg != '')
    require_once './app/modules/'. $arg .'.php';
else
{
    $getBooks = $db->query("SELECT bookId, bookName FROM books");
    $rows = '';
    for($i = 0; $i < $db->numRows($getBooks); $i++)
    {

        $data = $db->fetch($getBooks);
        $rows .= $tplMgr->Put(array('id' => $data[0], 'name' => $data[1], 'type' => 'book'), 'list/main/row');
    }
    $rows .= $tplMgr->Put(array('type' => 'book'), 'list/main/lastRow');
    $echo .= $tplMgr->Put($rows, 'list/main/frame');

    $getAuthors = $db->query("SELECT authorId, authorName FROM `authors`");
    $rows = '';
    for($i = 0; $i < $db->numRows($getAuthors); $i++)
    {
        $data = $db->fetch($getAuthors);
        $rows .= $tplMgr->Put(array('id' => $data[0], 'name' => $data[1], 'type' => 'author'), 'list/main/row');
    }
    $rows .= $tplMgr->Put(array('type' => 'author'), 'list/main/lastRow');
    $echo .= $tplMgr->Put($rows, 'list/main/frame');

}
$tplMgr->Output($echo);