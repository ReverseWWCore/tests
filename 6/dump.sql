/*
SQLyog Ultimate v12.4.1 (64 bit)
MySQL - 5.5.25 : Database - srv27643
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `authors` */

CREATE TABLE `authors` (
  `authorId` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `authorName` varchar(255) DEFAULT NULL,
  KEY `authorId` (`authorId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `authors` */

insert  into `authors`(`authorId`,`authorName`) values 
(1,'Author 1'),
(2,'Author 2'),
(3,'Author 3'),
(4,'test adding authors'),
(5,'author \'&quot;&lt;broken&gt;');

/*Table structure for table `books` */

CREATE TABLE `books` (
  `bookId` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `bookName` varchar(255) DEFAULT NULL,
  KEY `bookId` (`bookId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `books` */

insert  into `books`(`bookId`,`bookName`) values 
(1,'book'),
(2,'book with 2 authors'),
(3,'added book'),
(4,'test book 2');

/*Table structure for table `booksAuthors` */

CREATE TABLE `booksAuthors` (
  `bookId` int(11) unsigned NOT NULL,
  `authorId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`bookId`,`authorId`),
  KEY `authorId` (`authorId`),
  CONSTRAINT `booksAuthors_ibfk_1` FOREIGN KEY (`bookId`) REFERENCES `books` (`bookId`),
  CONSTRAINT `booksAuthors_ibfk_2` FOREIGN KEY (`authorId`) REFERENCES `authors` (`authorId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `booksAuthors` */

insert  into `booksAuthors`(`bookId`,`authorId`) values 
(1,1),
(2,1),
(3,1),
(4,1),
(1,2),
(2,2),
(3,2),
(4,2),
(1,3),
(3,3),
(1,4),
(2,4),
(3,4),
(4,4),
(1,5);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
