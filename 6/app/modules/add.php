<?php
/**
 * Created by PhpStorm.
 * User: Роман Аношкин
 * Date: 04.09.2017
 * Time: 20:07
 */
$type = '';
if(isset($_GET['book']))
    $type = 'book';
elseif(isset($_GET['author']))
    $type = 'author';

if($type == '')
    $echo .= $tplMgr->Put('!#null#!', 'add/specify');
else
{
    $showForm = !isset($_POST['save']);
    if(!$showForm) {
        $name = (isset($_POST[$type . 'Name'])) ? $db->esc($_POST[$type . 'Name']) : '';
        if ($name != '') {
            if ($db->query("INSERT INTO `" . $type . "s` VALUES (NULL, '" . $name . "')")) {
                $getMax = $db->query("SELECT max(" . $type . "Id) FROM `" . $type . "s`");
                $id = 0;
                if ($getMax) {
                    $data = $db->fetch();
                    $id = $data[0];
                }
                $echo .= $tplMgr->Put(array('type' => $type, 'id' => $id), 'add/msgAdded');
            } else {
                $echo .= $tplMgr->Put($type, 'add/msgNotAdded');
                $showForm = true;
            }
        }
    }
    if($showForm)
        $echo .= $tplMgr->Put($type, 'add/msgNotAdded');;
}
