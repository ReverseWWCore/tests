<?php
/**
 * Created by PhpStorm.
 * User: Роман Аношкин
 * Date: 04.09.2017
 * Time: 20:07
 */
if(isset($_GET['book']))
{
    $id = 0;
    if(is_numeric($_GET['book']))
        $id = $db->esc($_GET['book']);
    if($id > 0) {
        $db->query("START TRANSACTION");
        if (!$db->query("DELETE FROM `books` WHERE bookId = ". $id) || !$db->query("DELETE FROM `booksAuthors` WHERE bookId = ". $id)) {
            $db->query("ROLLBACK");
            $echo .= $tplMgr->Put('book', 'remove/msgNotRemoved');
        } else {
            $db->query("COMMIT");
            $echo .= $tplMgr->Put('book', 'remove/msgRemoved');
        }
    }
}
elseif(isset($_GET['author'])) {
    $id = 0;
    if (is_numeric($_GET['author']))
        $id = $db->esc($_GET['author']);
    if ($id > 0) {
        $db->query("START TRANSACTION");
        if (!$db->query("DELETE FROM `authors` WHERE authorId = " . $id) || !$db->query("DELETE FROM `booksAuthors` WHERE authorId = " . $id)) {
            $db->query("ROLLBACK");
            $echo .= $tplMgr->Put('author', 'remove/msgNotRemoved');
        } else {
            $db->query("COMMIT");
            $echo .= $tplMgr->Put('author', 'remove/msgRemoved');
        }
    }
}