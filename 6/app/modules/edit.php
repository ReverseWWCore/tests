<?php
/**
 * Created by PhpStorm.
 * User: Роман Аношкин
 * Date: 04.09.2017
 * Time: 20:09
 */
if(isset($_GET['book']))
{
    $id = isset($_GET['id']) ? $db->esc($_GET['id']) : 0;
    if(isset($_POST['save']))
    {
        if(isset($_POST['bookName'])) // edit book
        {
            if ($db->query("UPDATE books SET bookName = '" . $db->esc($_POST['bookName']) . "' WHERE bookId = " . $id . " LIMIT 1"))
                $echo .= '<div style="color: #0F0;">Book name edited</div><br />';
            else
                $echo .= '<div style="color: #F00;">Err while book name editing</div><br />';
        }
        elseif(isset($_POST['addAuthor'])) // add author
        {
            if ($db->query("INSERT INTO `booksAuthors` VALUES (" . $id . ", " . $db->esc($_POST['addAuthor']) . ")"))
                $echo .= '<div style="color: #0F0;">Author added to book info</div><br />';
            else
                $echo .= '<div style="color: #F00;">Err while adding author to book info</div><br />';
        }
    }

    if(isset($_GET['remAuthor']))
    {
        if ($db->query("DELETE FROM booksAuthors WHERE bookId = ". $id ." AND authorId = ". $db->esc($_GET['remAuthor']) ." LIMIT 1"))
            $echo .= '<div style="color: #0F0;">Author removed from book info</div><br />';
        else
            $echo .= '<div style="color: #F00;">Err while removing author from book info</div><br />';
    }

    $getData = $db->query("SELECT bookId, bookName FROM `books` WHERE bookId = ". $id ." LIMIT 1");
    if(!$getData)
        $echo .= '<div style="color: #F00;"> Err book not found</div>';
    else
    {
        $bookData = $db->fetch($getData);
        $echo .= $tplMgr->Put(array('type' => 'book', 'id' => $id, 'name' => $bookData[1]), 'edit/formName');

        $inner = '';

        $authorIds = '';

        $getAuthors = $db->query("SELECT a.authorId, b.authorName FROM `authors` b LEFT JOIN booksAuthors a ON b.authorId = a.authorId WHERE a.bookId = ". $id);
        if($getAuthors)
        {
            for($i = 0; $i < $db->numRows($getAuthors); $i++)
            {
                $aData = $db->fetch($getAuthors);
                $inner .= $tplMgr->Put(array('name' => $aData[1], 'type' => 'book', 'id'=> $id, 'remtype' => 'Author', 'remid' => $aData[0]), 'edit/sublist/row');
                if($authorIds != '')
                    $authorIds .= ', ';
                $authorIds .= $aData[0];
            }
        }
        if($authorIds == '')
            $authorIds = 0;
        $getAllUnusedAuthors = $db->query("SELECT authorId, authorName FROM `authors` WHERE authorId NOT IN (". $authorIds .")");
        $aInner = '';
        if($getAllUnusedAuthors)
            for($i = 0; $i < $db->numRows(); $i++)
            {
                $addAData = $db->fetch();
                $aInner .= $tplMgr->Put(array('id' => $addAData[0], 'value' => $addAData[1]), 'edit/sublist/addRowInner');
                $inner .= $tplMgr->Put(array('type' => 'book', 'id' => $id, 'addtype' => 'Author', 'inner' => $aInner), 'edit/sublist/addRow');
            }
        $echo .= $tplMgr->Put(array('type' => 'Authors', 'rows' => $inner), 'edit/sublist/frame');
    }
}
elseif(isset($_GET['author']))
{
    $id = isset($_GET['id']) ? $db->esc($_GET['id']) : 0;
    if(isset($_POST['save']))
    {
        if(isset($_POST['authorName']))
        {
            if ($db->query("UPDATE `authors` SET authorName='" . $db->esc($_POST['authorName']) . "' WHERE authorId = " . $id))
                $echo .= '<div style="color: #0F0;">Author name edited</div><br />';
            else
                $echo .= '<div style="color: #F00;">Err while editing author name</div><br />';
        }
        elseif(isset($_POST['addToBook']))
        {
            if ($db->query("INSERT INTO `booksAuthors` VALUES (" . $db->esc($_POST['addToBook']) . ", " . $id . ")"))
                $echo .= '<div style="color: #0F0;">Author added to book info</div><br />';
            else
                $echo .= '<div style="color: #F00;">Err while adding author to book info</div><br />';
        }
    }
    if(isset($_GET['remBook']))
    {
        if ($db->query("DELETE FROM booksAuthors WHERE bookId = ". $db->esc($_GET['remBook']) ." AND authorId = ". $id ." LIMIT 1"))
            $echo .= '<div style="color: #0F0;">Author removed from book info</div><br />';
        else
            $echo .= '<div style="color: #F00;">Err while removing author from book info</div><br />';
    }
    $getData = $db->query("SELECT authorId, authorName FROM `authors` WHERE authorId = ". $id);
    if(!$getData)
        $echo .= '<div style="color: #F00;">Err author not found</div>';
    else
    {
        $data = $db->fetch($getData);
        $echo .= $tplMgr->Put(array('type' => 'author', 'id' => $id, 'name' => $data[1]), 'edit/formName');

        $inner = '';

        $usedBooksIds = '';

        $getBooks = $db->query("SELECT a.bookId, a.bookName FROM books a LEFT JOIN booksAuthors b ON a.bookId = b.bookId WHERE authorId = ". $id);
        if($getBooks)
        {
            for($i = 0; $i < $db->numRows(); $i++)
            {
              $data = $db->fetch();
                $inner .= $tplMgr->Put(array('name' => $data[1], 'type' => 'author', 'id'=> $id, 'remtype' => 'Book', 'remid' => $data[0]), 'edit/sublist/row');
                if($usedBooksIds != '')
                    $usedBooksIds .= ', ';
                $usedBooksIds .= $data[0];
            }
            if($usedBooksIds == '')
                $usedBooksIds = 0;
            $getUnusedBooks = $db->query("SELECT bookId, bookName FROM books WHERE bookId NOT IN (". $usedBooksIds .")");
            $aInner = '';
            if($getUnusedBooks) {
                for ($i = 0; $i < $db->numRows($getUnusedBooks); $i++) {
                    $uData = $db->fetch($getUnusedBooks);
                    $aInner .= $tplMgr->Put(array('id' => $uData[0], 'value' => $uData[1]), 'edit/sublist/addRowInner');
                }
                $inner .= $tplMgr->Put(array('type' => 'author', 'id' => $id, 'addtype' => 'ToBook', 'inner' => $aInner), 'edit/sublist/addRow');
            }
            $echo .= $tplMgr->Put(array('type' => 'Books', 'rows' => $inner), 'edit/sublist/frame');
        }
    }
}