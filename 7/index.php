<?php
/**
 * Created by PhpStorm.
 * User: Роман Аношкин
 * Date: 05.09.2017
 * Time: 0:37
 */
$books = array(
				0 => array(
				'name' => 'book 1',
				'authors' => 'a1, a2',
				),
				1 => array(
				'name' => 'book 2',
				'authors' => 'a2, a3',
				),
				2 => array(
				'name' => 'book 3',
				'authors' => 'a1, a2, --',
				),
				3 => array(
				'name' => 'book 4',
				'authors' => 'a1, a2, ----',
				),
				4 => array(
				'name' => 'book 5',
				'authors' => 'a1, a2, -----',
				)
			);

echo '<table border="1">
	<thead>
	<tr><th>name</th><th>authors</th></tr>
	</thead>
	<tbody>';
foreach($books as $id => $data)
	echo '<tr><td>'. $data['name'] .'</td><td>'. $data['authors'] .'</td></tr>';
echo '</tbody></table><br />Struct:<br />';
print_r($books);