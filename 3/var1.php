<?php
/**
 * Created by PhpStorm.
 * User: Роман Аношкин
 * Date: 05.09.2017
 * Time: 0:05
 */
/*
 * Вариант решения 1: Средствами PHP
 * Произвести проверку по REMOTE_ADDR
 */
echo 'Вариант решения 1: Средствами PHP<br />
 
  Произвести проверку по REMOTE_ADDR<br />';

$allowedIp = '127.0.0.1';
if($_SERVER['REMOTE_ADDR'] != $allowedIp)
    exit('Ваш IP не '. $allowedIp .', вам войти нельзя'); // грубый выход, можно заменить редиректом на 403
