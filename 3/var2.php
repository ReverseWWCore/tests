<?php
/**
 * Created by PhpStorm.
 * User: Роман Аношкин
 * Date: 05.09.2017
 * Time: 0:12
 */
/*
 * Второй, альтернативный вариант - создание правил .htaccess
 *
 *  order deny,allow // выставление порядка
    deny from all    // Реджект всех входящих подключений
    allow from {$targetIp} // Правило допускающее подключения для $targetIp
 */
echo 'Второй, альтернативный вариант - создание правил .htaccess<br />
 <br />
   order deny,allow // выставление порядка <br />
    deny from all    // Реджект всех входящих подключений<br />
    allow from {$targetIp} // Правило допускающее подключения для $targetIp<br />
	<br />
	при таком решении реджект происходит силами Apache/Nginx, без доступа к модулю PHP
	';
echo '<a href="./var2.php?lock">Lock</a> | <a href="./var2.php?unlock">Unlock</a>';
if(isset($_GET['lock'])) {
    echo ' Доступ разрешен только с адреса: '. $_SERVER['REMOTE_ADDR'];
    file_put_contents('./.htaccess', "order deny,allow\ndeny from all\nallow from " . $_SERVER['REMOTE_ADDR']);
}
if(isset($_GET['unlock'])) {
    echo ' Доступ разрешен только всем';
    file_put_contents('./.htaccess', "order allow,deny\nallow from all");
}